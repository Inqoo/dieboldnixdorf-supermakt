angular.module('app', [])

angular.module('app').component('basket', {
  template: /* html */`
    <h4>Your developers:</h4>
    <div class="list-group"'>
      <div class="list-group-item" ng-repeat="item in $ctrl.basket">
        {{item.name}} - {{item.price}}
      </div>
    </div>
    <strong>Total: {{$ctrl.total}}</strong>
  `,
  controller($rootScope, $scope) {
    this.basket = $rootScope.basket = [
      // { name: 'test', price: 123 }
    ]
    $scope.$watchCollection('$root.basket', () => {
      this.total = this.basket.reduce((sum, item) => sum += item.price, 0)
    })
  }
})

// <add-to-basket name="Zbyszek" price="100" ></add-to-basket>
angular.module('app').component('addToBasket', {
  bindings: {
    name: '@',
    price: '=',
  },
  template: /* html */`
    <button ng-click="$ctrl.add()">Add To Cart</button>
  `,
  controller($rootScope) {
    this.add = () => {
      $rootScope.basket.push({ name: this.name, price: this.price })
    }
  }
})
